
from app.app_factory import create_app
from app.settings import EnvConfiguration

print('Running wsgi.py')
application = create_app(EnvConfiguration)
