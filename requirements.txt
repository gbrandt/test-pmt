blinker==1.4
gunicorn==19.9.0
Flask==1.0.2
Flask-Env==2.0.0
Flask-SQLAlchemy==2.3.2
Flask-Admin==1.5.3
toml==0.10.0
#flask_sso
#flask-appbuilder
#dash
#ERAlchemy==1.2.10 

