
import logging

from flask import Flask, request

from app.extensions import db
import app.pmt.pmt as pmt

#import dash
#import dash_renderer
#import dash_core_components as dcc
#import dash_html_components as html

logging.basicConfig(level=logging.DEBUG)

def create_app(config_filename):
    """
    Factory to create the application using a file

    :param config_filename: The name of the file that will be used for configuration.
    :return: The created application
    """

    app = Flask(__name__, template_folder='templates/')
    app.config.from_object(config_filename)
    #app.config.from_pyfile('../web-variables.env')
    #app.config['EXPLAIN_TEMPLATE_LOADING'] = True

    #for k in app.config:
    #    print(k, ':', app.config[k])

    # setup_db(app)

    print("Creating a Flask app with DEBUG: {}".format(app.debug))

    # embed Dash app
    #dash_app = dash.Dash(__name__, server=app, url_base_pathname='/dash/')
    # dash_app.layout = html.Div([
    #    html.H1('Hello Dash'),
    #    html.Div('''
    #        Dash: A web application framework for Python.
    #    '''),
    #    html.H1('Dash app here'),
    # ])

    # Pixel Module Tracker module
    pmt.init_db(app)
    pmt.init(app)

    # Flask views
    @app.route('/')
    def index():
        """Display user information or force login."""
        for h in request.headers:
            print(h)
        # if 'user' in session:
        #    return f"Welcome {session['user']['nickname']}"
        return '''
            <h1>ATLAS Pixel Web Applications</h1>
            <div>
                <a href="/pmt/">Click me to get to Pixel Module Tracker</a>
            </div>
            <div>
                <a href="/dash/">Click me to get to Dash!</a>
            </div>
            <div>
                DEBUG: {}
            </div>
            <div>
                Environment: {} PIXEL
            </div>
        '''.format(app.debug, app.env)

    # @app.route("/dash")
    # def MyDashApp():
    #    return dash_app.index()

    return app


if __name__ == '__main__':

    # create initial db, if one does not exist yet.
    app_dir = os.path.realpath(os.path.dirname(__file__))
    database_path = os.path.join(app_dir, app.config['DATABASE_FILE'])
    if not os.path.exists(database_path):
        init_db()

    app.run(debug=True)
