
from flask import url_for, Markup
from app.extensions import db

# Helper tables for many-to-many relations

history_actions = db.Table('history_actions',
    db.Column('action_id', db.Integer, db.ForeignKey('actions.id')),
    db.Column('history_id', db.Integer, db.ForeignKey('history.id'))
)

history_symptoms = db.Table('history_symptoms',
    db.Column('symptom_id', db.Integer, db.ForeignKey('symptoms.id')),
    db.Column('history_id', db.Integer, db.ForeignKey('history.id'))
)

history_hypothesis = db.Table('history_hypothesis',
    db.Column('hypothesis_id', db.Integer, db.ForeignKey('hypothesis.id')),
    db.Column('history_id', db.Integer, db.ForeignKey('history.id'))
)

history_diagnosis = db.Table('history_diagnosis',
    db.Column('diagnosis_id', db.Integer, db.ForeignKey('diagnosis.id')),
    db.Column('history_id', db.Integer, db.ForeignKey('history.id'))
)

class ItemsModel(db.Model):
    """Pixel Module Tracker Item Tables Base Class
    """
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    label = db.Column(db.String, unique=True, nullable=False)

    def __repr__(self):
        return str(self.label)

class Actions(ItemsModel):
    pass

class Hypothesis(ItemsModel):
    pass

class Diagnosis(ItemsModel):
    pass

class Symptoms(ItemsModel):
    pass

class Priorities(ItemsModel):
    pass


class PixModule(db.Model):
    """ Module Table (for one-to-many relationship into History Table)
    """
    __tablename__='pixmodule'
    id = db.Column(db.Integer, primary_key=True)

    conn_id = db.Column(db.Integer, db.ForeignKey('connectivity.id'))
    conn = db.relationship('Connectivity', backref=db.backref('pixmod', uselist=False))

    histories = db.relationship('History', backref = 'pixmod')

    def __repr__(self):
        return f'{self.conn}'


class Connectivity(db.Model):
    """Connectivity Table
    """
    id = db.Column(db.Integer, primary_key=True)
    mid = db.Column(db.String, unique=False, nullable=False)
    name = db.Column(db.String, unique=False, nullable=False)
    rod = db.Column(db.String, unique=False, nullable=False)

    #pixmod = db.relationship('PixModule', backref='conn', uselist=False, unique=True)

    def __repr__(self):
        return f'<{self.mid}> {self.name} ({self.rod})'


class History(db.Model):
    """ Master History Table
    """
    #__versioned__ = {}
    id = db.Column(db.Integer, primary_key=True)

    pixmod_id = db.Column(db.Integer, db.ForeignKey('pixmodule.id'))
    #pixmod = db.relationship('PixModule', backref='history')

    #moduleid_id = db.Column(db.Integer, db.ForeignKey('connectivity.id'), nullable=False)
    #moduleid = db.relationship('Connectivity', backref='modules')

    observed = db.Column(
        db.DateTime, unique=False, nullable=False,default=db.func.current_timestamp(),
    )

    actions = db.relationship('Actions', secondary=history_actions, backref='modules')

    priority_id = db.Column(db.Integer, db.ForeignKey('priorities.id')) #, nullable=False)
    priority = db.relationship('Priorities',  backref='modules')

    symptoms = db.relationship('Symptoms', secondary=history_symptoms, backref='modules')
    hypothesis = db.relationship('Hypothesis', secondary=history_hypothesis, backref='modules')
    diagnosis = db.relationship('Diagnosis', secondary=history_diagnosis, backref='modules')

    comment = db.Column(db.String, unique=False)#, nullable=False)
    username = db.Column(db.String, unique=False)#, nullable=False)
    systemdate = db.Column(db.DateTime, unique=False, nullable=False,
        default = db.func.current_timestamp(),
        onupdate = db.func.current_timestamp()
    )

    def __repr__(self):
        obs = str(self.observed).split('-',1)[1].rsplit(':',1)[0]
        return f'<{self.pixmod}> ({obs})'

