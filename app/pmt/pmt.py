
import os
import shlex
import toml

try:
    from eralchemy import render_er
except ImportError:
    eralchemy_loaded = False
else:
    eralchemy_loaded = True

from flask_admin import Admin, AdminIndexView
from flask_admin.base import MenuLink

from app.extensions import db
from app.pmt.models import *
from app.pmt.views import *

# TOOLS & HACKS

#########
# Hack for hardcoded admin.static in default templates
# https://github.com/flask-admin/flask-admin/issues/943

#import flask_admin.base
#
#_get_url = flask_admin.base.BaseView.get_url
#_create_blueprint = flask_admin.base.BaseView.create_blueprint
#
#def get_url(self, endpoint, **kwargs):
#    if endpoint == 'admin.static':
#        endpoint = '{}.static'.format(self.admin.endpoint)
#    return _get_url(self, endpoint, **kwargs)
#
#def create_blueprint(self, admin):
#    if self.endpoint == admin.endpoint:
#        self.endpoint = admin.endpoint
#    else:
#        self.url = self._get_view_url(admin, self.url)
#        self.endpoint = '{}.{}'.format(admin.endpoint, self.endpoint)
#    return _create_blueprint(self, admin)
#
#flask_admin.base.BaseView.get_url = get_url
#flask_admin.base.AdminIndexView.get_url = get_url
#flask_admin.base.BaseView.create_blueprint = create_blueprint
#flask_admin.base.AdminIndexView.create_blueprint = create_blueprint

##########

def get_or_create(session, model, **kwargs):
    """Get, or if not exists, create model instance from db session
    """
    instance = db.session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance
    else:
        instance = model(**kwargs)
        session.add(instance)
        session.commit()
        return instance


def init(app):
    """Create flask-admin Admin app for Pixel Module Tracker
    """

    app.logger.info('init Admin')

    app.config['FLASK_ADMIN_SWATCH'] = 'cerulean'
    #'cyborg' #'darkly'
    #'lumen' #flatly' #slate' #cosmo' #superhero'

    pmt = Admin(app,
        name='Pixel Module Tracker',
        endpoint='pmt',
        url='/pmt',
        static_url_path='/static/pmt',
        base_template='pmt.html',
        template_mode='bootstrap3',
        index_view = AdminIndexView(
            name='Info', template='pmt/index.html', url='/pmt'
        ),
    )
    pmt.add_links(
        MenuLink(name='CERN Account',url='https://cern.ch/account'),
        MenuLink(name='Sign out',url='https://home.cern/user/logout'),
        MenuLink(name='Pixel Webapps',url='/'),
    )
    pix_items = 'Pixel Items'
    pmt.add_views(
        ConnectivityModelView(Connectivity, db.session),
        PixModuleView(PixModule, db.session),
        HistoryModelView(History, db.session),
        ItemsModelView(Actions, db.session, category=pix_items),
        ItemsModelView(Hypothesis, db.session, category=pix_items),
        ItemsModelView(Diagnosis, db.session, category=pix_items),
        ItemsModelView(Symptoms, db.session, category=pix_items),
        ItemsModelView(Priorities, db.session, category=pix_items),
    )


def init_db(app):
    """
    Create initial PMT db structure.
    """

    # create initial db, if one does not exist yet.

    _basedir = os.path.abspath(os.path.dirname(__file__))

    #print(os.listdir(_basedir))

    # database_path = os.path.join(_basedir, 'webapp.db') #app.config['DATABASE_FILE'])
    #app.config["SQLALCHEMY_DATABASE_URI"] = 'sqlite:///' + database_path
    app.config["SQLALCHEMY_DATABASE_URI"] = 'sqlite://'
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config["SECRET_KEY"] = 'mysecret'

    db.init_app(app)
    db.app = app

    app.logger.info('creating db')

    db.drop_all()
    db.create_all()

    # fill item tables
    app.logger.info('load item tables')

    p = toml.load(os.path.join(_basedir, 'pixelmod.toml'))
    # print(p)

    def load_items(table_cls, table_name):
        for l in p[table_name]:
            s = table_cls()
            s.label = l
            db.session.add(s)
        db.session.commit()
        #print(table_name, l)

    load_items(Symptoms, 'symptoms')
    load_items(Hypothesis, 'diagnosis')
    load_items(Diagnosis, 'diagnosis')
    load_items(Actions, 'actions')
    load_items(Priorities, 'priorities')

    # fill Connectivity table
    app.logger.info('load Connectivity')

    with open(os.path.join(_basedir, 'modlist.txt')) as f:
        content = f.readlines()
        for l in content[1:]:
            l = l.split()
            c = Connectivity()
            c.mid = l[1]
            c.rod = l[0]
            c.name = l[2]
            db.session.add(c)
        db.session.commit()

    # fill History table from old history table dump
    #app.logger.info('load Pixel Module table')
    app.logger.info('load History table')

    # fill History table from old history table dump

    def get_items_list(item_table, tokens):
        return [
            db.session.query(item_table).filter(item_table.label==s).one() for s in tokens.split(',')
        ]

    app.logger.info('load old database')
    with open(os.path.join(_basedir, 'test.dump')) as f:
        content = f.readlines()
        for l in content[1:]:
            l = shlex.split(l)
            #app.logger.info(l)

            h = History()

            #h.moduleid = db.session.query(Connectivity).filter(Connectivity.mid==l[1]).one()
            h.observed = datetime.strptime(l[3], '%Y-%m-%d %H:%M:%S')
            h.actions = get_items_list(Actions,l[5])
            priority = db.session.query(Priorities).filter(Priorities.label.in_(l[6].split(','))).first()
            #print(priority)
            #print(type(priority))
            h.priority = priority # db.session.query(Priorities).filter(Priorities.label.in_(l[6].split(','))).first()
            h.symptoms = get_items_list(Symptoms, l[7])
            h.hypothesis = get_items_list(Hypothesis,l[8])
            h.diagnosis = get_items_list(Diagnosis, l[9])
            h.comment = l[10] if len(l)>10 else ''
            h.username = l[4]
            h.systemdate = datetime.strptime(l[2], '%Y-%m-%d %H:%M:%S')
            db.session.add(h)

            # add history record to pixmodule table
            pm = get_or_create(
                db.session, PixModule,#name=l[1])
                conn = db.session.query(Connectivity).filter(Connectivity.mid==l[1]).one()
            )
            #h.pixmod_id = pm
            pm.histories.append(h)

        db.session.commit()

    if eralchemy_loaded:
        app.logger.info('Plot ERD')
        erd_path = os.path.join(_basedir, '../static/erd.png')
        render_er(PixModule, erd_path)
        #print(erd_path)

    app.logger.info('db initialized')

if __name__ == "__main__":
#    init_db()
    pass