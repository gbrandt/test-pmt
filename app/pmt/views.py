

import functools
import getpass
from datetime import datetime

from flask import Markup, url_for, request
from flask_admin.contrib.sqla import ModelView
from flask_admin.tools import rec_getattr
from flask_admin.form import rules
from flask_admin.base import expose
from flask_admin.helpers import get_redirect_target
from flask_admin.model.helpers import get_mdict_item_or_list

from app.pmt.models import History, PixModule


def debug(func):
    """Print the function signature and return value"""
    @functools.wraps(func)
    def wrapper_debug(*args, **kwargs):
        args_repr = [repr(a) for a in args]                      # 1
        kwargs_repr = [f"{k}={v!r}" for k, v in kwargs.items()]  # 2
        signature = ", ".join(args_repr + kwargs_repr)           # 3
        #print(f"Calling {func.__name__}({signature})")
        value = func(*args, **kwargs)
        print(f"{func.__name__!r} returned {value!r}")           # 4
        return value
    return wrapper_debug

# Column Formatters for list links back to respective table views

# @debug


class PixModuleView(ModelView):
    """PixModule table"""
    #column_display_pk = True
    column_auto_select_related = True
    column_list = ['conn', 'histories', ]
    column_labels = {
        'conn': 'Module',  # <ID> Connection (ROD)',
        'histories': 'History Entries',
    }
    column_searchable_list = [
        # ('mid','pixmod.mid'),
    ]
    column_filters = [
        'conn', 'histories'
    ]
    column_sortable_list = [
        ('conn', 'conn.mid'),
    ]
    #column_filters = ['mid']
    #column_details_list = ['conn', 'histories', ]
    can_view_details = True
    details_template = 'pmt/pixmodule_details.html'
    page_size = 50
    can_set_page_size = True
    inline_models = (History, )

    #@debug
    def details_link_formatter(view, context, model, name):
        """Create Links back to Pixmod details"""
        conn = getattr(model, name)
        #print(conn.pixmod.id)
        url = url_for('pixmodule.details_view', id=conn.pixmod.id)
        return Markup(f'<a href="{url}">')+repr(conn)+Markup('</a>')

    def history_link_formatter(view, context, model, name):
        """Create Links back to entries in History details"""
        field_list = getattr(model, name)
        links = ''
        for field in field_list:
            url = url_for('history.details_view', id=field.id)
            fstr = str(field).rsplit('(', 1)[1][:-1]
            links += f'<a href="{url}">{fstr}</a>, '
        return Markup(links)

    column_formatters = {
        'conn': details_link_formatter,
         'histories': history_link_formatter,
    }
    # reset column formatters for details_view
    column_formatters_detail = {}
    #form_rules = [
    #    rules.FieldSet(
    #        ('mid','histories', 'pixmod'),
    #        'PixModule'
    #    )
    #]

    def get_save_return_url(self, model, is_created):
        return self.get_url('.details_view', id=model.id)

    #@debug
    @expose('/details/')
    def details_view(self):
        """Custom details view.
        """
        return_url = get_redirect_target() or self.get_url('.index_view')

        id = get_mdict_item_or_list(request.args, 'id')
        if id is None:
            return redirect(return_url)

        model = self.get_one(id)

        if model is None:
            flash(gettext('Record does not exist.'), 'error')
            return redirect(return_url)

        #print('Model',dir(model))

        history_column_list = [
            'observed', 'actions', 'priority',
            'symptoms', 'hypothesis', 'diagnosis',
            'comment', 'username', 'systemdate',
        ]

        histories = {}
        for history in model.histories:
            hdict = {}
            for name in history_column_list:
                field = getattr(history,name)
                hdict[name] = field
            histories[repr(history)] = dict(hdict)

        template = self.details_template

        return self.render(
            template,
            module = repr(model.conn),
            histories = histories,
            return_url=return_url,
        )



class ConnectivityModelView(ModelView):
    """
    """
    #column_display_pk = True
    #column_display_all_relations = True
    column_list = ['mid', 'name', 'rod', 'pixmod']
    column_filters = ['mid', 'name', 'rod']
    column_labels = {
        'mid': 'Module ID',
        'name': 'Name',
        'rod': 'ROD',
        'pixmod': 'Pix Module',
    }
    column_descriptions = {
        'mid': 'Module serial number.',
        'name': 'Connection name.',
        'rod': 'ROD crate and slot.',
        'pixmod': 'Link to Pix Module table (if entries exist).'
    }
    page_size = 50
    can_set_page_size = True
    can_delete = False
    can_edit = False
    can_create = False

    def pixmod_link_formatter(view, context, model, name):
        """Create Link back to PixModule details"""
        field = getattr(model, name)
        # print(type(field).__name__)
        if isinstance(field, type(None)):
            return Markup('')
        url = url_for('pixmodule.details_view', id=field.id)
        fstr = str(field)  # .rsplit('(',1)[1][:-1]
        link = f'<a href="{url}">{fstr}</a>'
        return Markup(link)

    column_formatters = {
        'pixmod': pixmod_link_formatter,
    }


class ItemsModelView(ModelView):
    """
    """
    #column_display_pk = True
    column_auto_select_related = True
    column_list = [
        #'id',
        'label',
        'modules',
    ]
    #column_searchable_list = ['history',]
    page_size = 50
    can_set_page_size = True
    can_delete = False

    # @debug
    def user_link_formatter(view, context, model, name):
        field_list = getattr(model, name)
        links = ''
        for field in field_list:
            url = url_for('history.details_view', id=field.id)
            links += f'<a href="{url}">{field}</a>, '
        return Markup(links)

    column_formatters = {
        'modules': user_link_formatter,
    }


class HistoryModelView(ModelView):
    """
    """
    column_display_pk = True
    column_list = [
        # 'id',
        'pixmod',  # 'moduleid',
        'observed', 'actions', 'priority',
        'symptoms', 'hypothesis', 'diagnosis',
        'comment', 'username', 'systemdate',
    ]
    column_descriptions = {
        # 'moduleid'
        'pixmod': 'Module',
        'observed': 'Date/time action has been performed on module.',
        'actions': 'Actions performed on module at observed date.',
        'systemdate': 'Date/time entry has been edited last.',
    }
    column_labels = {
        'username': 'Author(s)',
    }
    column_searchable_list = [
        'comment',
    ]
    column_sortable_list = [
        # 'id',
        #('moduleid', 'moduleid.mid'),
        #('pixmod', 'pixmod_id.mid'),
        'observed',
        ('actions', 'actions.label'),
        ('priority', 'priority.label'),
        ('symptoms', 'symptoms.label'),
        ('hypothesis', 'hypothesis.label'),
        ('diagnosis', 'diagnosis.label'),
        'comment', 'username', 'systemdate',
    ]
    column_default_sort = 'observed'
    column_editable_list = [
        # 'moduleid','observed',
        'actions', 'priority',
        'symptoms', 'hypothesis', 'diagnosis',
        'comment',  # 'username','systemdate',
    ]
    column_filters = [
        # 'moduleid',
        # 'pixmod',
        'observed', 'actions', 'priority',
        'symptoms', 'hypothesis', 'diagnosis',
        'comment', 'username', 'systemdate',
    ]
    page_size = 50
    can_set_page_size = True
    #create_modal = True
    #edit_modal = True
    can_export = True
    #details_modal = True
    can_view_details = True
    username = getpass.getuser()
    form_args = {
        'priority': {'default': 'YETS', },
        'observed': {'default': datetime.now(), },
        'username': {'default': username, },
        'systemdate': {
            'default': datetime.now(),
            # 'readonly':True ,
        },
        # 'diagnosis':{'default':'Unknown'},
        # 'module id':{
        #    'query_factory': lambda: User.query_
        # }
    }
    form_widget_args = {
        'systemdate': {
            'readonly': True,
        },
    }
    form_create_rules = [
        rules.Header('Module'),
        'pixmod', 'observed',
        rules.Header('Items'),
        'actions', 'priority',
        'symptoms', 'hypothesis', 'diagnosis',
        'comment',
        rules.Header('Who'),
        'username', 'systemdate',
    ]
